function showNote(msg, status) {
	if (status == 'failure') status = 'error';
	$.noty({
		animateOpen: {
			opacity: 'show'
		}, 
		animateClose: {
			opacity: 'hide'
		}, 
		layout : 'topCenter',
		theme : 'noty_theme_default', 
		type : status,    
		text: msg
	});
}
function validTransaction()
{
	if ($('#saveTransaction #store_id').val() == '0') {
		showNote('Please select Store ID', 'warning');
	} else if ($('#saveTransaction #tx_name').val() == '0') {
		showNote('Please select Transaction Name', 'warning');
	} else if ($('#saveTransaction #tx_ref').val() == '') {
		showNote('Please enter Transaction Reference', 'warning');
	} else if ($('#saveTransaction #tx_type').val() == '0') {
		showNote('Please select Transaction Type', 'warning');
	} else if ($('#saveTransaction #tx_amt').val() == '') {
		showNote('Please enter Amount', 'warning');
	} else if ($('#saveTransaction #tx_fee').val() == '') {
		showNote('Please enter Fee', 'warning');
	} else {
		return false;
	}
	return true;
}
function validAddCustomer()
{
	if ($('#addCustomerForm #firstname').val() == '') {
		showNote('Please enter Firstname', 'warning');
	} else if ($('#addCustomerForm #lastname1').val() == '') {
		showNote('Please enter Lastname', 'warning');
	} else if ($('#addCustomerForm #birth_date').val() == '') {
		showNote('Please enter Birth Date', 'warning');
	/*} else if ($('#addCustomerForm #ssn_id').val() == '') {
		showNote('Please enter SSN ID', 'warning');*/
	} else if ($('#addCustomerForm input[name=identity_type]:checked').length == 0) {
		showNote('Please select Identification', 'warning');
	} else if ($('#addCustomerForm #identity_id').val() == '') {
		showNote('Please enter Identity ID', 'warning');
	} else if ($('#addCustomerForm #identity_issue_state').val() == '') {
		showNote('Please enter Identity State', 'warning');
	} else if ($('#addCustomerForm #identity_exp').val() == '') {
		showNote('Please enter Identity Expiry Date', 'warning');
	} else {
		return false;
	}
	return true;
}